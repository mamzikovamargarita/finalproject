package stepdefinitions;

import io.cucumber.datatable.internal.difflib.StringUtills;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.HomePage;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DefinitionSteps {

    private static final long DEFAULT_TIMEOUT = 60;

    WebDriver driver;
    HomePage homePage;
    PageFactoryManager pageFactoryManager;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @And("User opens {string} page")
    public void openPage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @When("User switch to russian language by clicking link")
    public void userSwitchToRussianLanguageByClickingLink() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.languageLinkClick();
    }

    @Then("User verifies that the text of the first  element, which is the first paragraph, contains the word {string}")
    public void userVerifiesThatTheTextContainsTheWord(final String expectedText) {
        assertEquals("рыба", expectedText);
    }

    @When("User press “Generate Lorem Ipsum”")
    public void userPressGenerateLoremIpsum() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.generateLoremIpsumButtonClick();
    }

    @Then("User verifies that the first paragraph starts with {string}")
    public void userVerifiesThatTheFirstParagraphStartsWith(final String expectedSentence) {
        assertEquals("Lorem ipsum dolor sit amet, consectetur adipiscing elit", expectedSentence);

    }


    @And("User clicks on {string}")
    public void userClicksOnWordsButton(String arg0) {
        homePage.wordsButtonClick();
    }


    @When("User input {string} into the number field")
    public void userInputIntoTheNumberField(final String input) {
        homePage.enterNumberToInputField(input);
    }


    @Then("User verifies that result has {string} words")
    public void userVerifiesThatResultHasResultWords(final int expectedNumber) {
        assertTrue(homePage.verifyCorrectGeneration(expectedNumber));

    }
}
