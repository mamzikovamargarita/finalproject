package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


import java.util.List;


public class HomePage extends BasePage {

    @FindBy(xpath = "//a[@href][contains(text(), 'Pyccкий')]")
    private WebElement languageLink;

    @FindBy(xpath = "//input[@value = 'Generate Lorem Ipsum']")
    private WebElement generateLoremIpsumButton;

    @FindBy(xpath = "//p[contains(text(), 'рыба')]")
    private WebElement textOnPageTranslated;

    @FindBy(xpath = "//p[contains(text(), 'Lorem ipsum dolor sit amet')]")
    private WebElement textOnPageGenerated;

    @FindBy(xpath = "//label[@for = 'words']")
    private WebElement wordsButton;

    @FindBy(xpath = "//input[@id = 'amount']")
    private WebElement inputAmountField;

    @FindBy(xpath = "//input[@id = 'start']")
    private WebElement startWIthLoremIpsumCheckbox;


    @FindBy(xpath = "//label[@for = 'bytes']")
    private WebElement bytesButton;



    @FindBy(xpath = "//div[@id = 'generated']")
    private List<WebElement> generatedText;


    public void languageLinkClick() {
        languageLink.click();
    }

    public void generateLoremIpsumButtonClick() {
        generateLoremIpsumButton.click();
    }

    public String getTextOnPageTranslated() {
        return textOnPageTranslated.getText();
    }

    public String getTextOnPageGenerated() {
        return textOnPageGenerated.getText();
    }

    public void wordsButtonClick() {
        wordsButton.click();
    }

    public void startWIthLoremIpsumCheckboxClick() {
        startWIthLoremIpsumCheckbox.click();
    }

    public void bytesButtonClick() {
        bytesButton.click();
    }


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage(String url) {
        driver.get(url);
    }

    public void enterNumberToInputField(final String searchNumber) {
        inputAmountField.clear();
        inputAmountField.sendKeys(searchNumber);
    }

    public boolean verifyCorrectGeneration(int input) {
        if (input > 3) {
            generatedText.contains(input);
        } else generatedText.contains(3);
        return false;
    }

}
